unit quadratic;
  { a unit to find roots of quadratic polynomial}
  interface
    function discriminantFind(a,b,c: real): real;
    function discriminantPositive(discriminant: real): integer;
    function solutionOnePositive(a,b,discriminant: real): real;
    function solutionOtherPositive(a,b,discriminant: real): real;
    function solutionBNegative(a,b: real): real;
    function solutionDiscriminantNegative(a,discriminant: real): real;
  implementation
    function discriminantFind(a,b,c: real): real;
    begin
      discriminantFind := b*b-4*a*c;
    end;
    function discriminantPositive(discriminant: real): integer;
    begin
      if (discriminant >= 0) then
        discriminantPositive := 1
      else
        discriminantPositive := 0;
    end;
    function solutionOnePositive(a,b,discriminant: real): real;
    begin
      solutionOnePositive := (-b+Sqrt(discriminant))/(2*a);
    end;
    function solutionOtherPositive(a,b,discriminant: real): real;
    begin
      solutionOtherPositive := (-b-Sqrt(discriminant))/(2*a);
    end;
    function solutionBNegative(a,b: real): real;
    begin
      solutionBNegative := (-b)/(2*a);
    end;
    function solutionDiscriminantNegative(a,discriminant: real): real;
    begin
      solutionDiscriminantNegative := Sqrt(Abs(discriminant))/(2*a);
    end;
end.
